package com.ias.hourscalculatorapi.commons;

import lombok.Data;

@Data
public class HourReport {
    private Float normalHours;
    private Float sundayHours;
    private Float extraNormalHours;
    private Float nightHours;
    private Float extraSundayHours;
    private Float extraNightHours;
}

package com.ias.hourscalculatorapi.handler;

import com.ias.hourscalculatorapi.service.HoursCalculatorService;
import com.ias.hourscalculatorapi.commons.HourReport;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class HoursCalculatorHandler {

    private final HoursCalculatorService hoursCalculatorService;

    public Mono<ServerResponse> hoursCalculator(ServerRequest request) {
        String technicianId = request.pathVariable("technicianId");
        Integer weekNumber = Integer.parseInt(request.pathVariable("weekNumber"));
        Mono<Object> hourReport = hoursCalculatorService.calculateHours(technicianId, weekNumber)
                .map(h -> h.get(0));
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(hourReport, HourReport.class);
    }
}

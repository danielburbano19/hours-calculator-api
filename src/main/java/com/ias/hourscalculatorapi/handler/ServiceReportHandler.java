package com.ias.hourscalculatorapi.handler;

import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.service.ServiceReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.net.URI;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class ServiceReportHandler {

    private final ServiceReportService serviceReportService;

    public Mono<ServerResponse> saveServiceReport(ServerRequest request) {
        Mono<ServiceReport> serviceReport = request.bodyToMono(ServiceReport.class);
        return serviceReport.flatMap(sr -> {
            Map<String, Object> constraintViolation = validateRequest(sr);
            if (constraintViolation.containsKey("error")) {
                return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        constraintViolation.get("error").toString()));
            }
            return serviceReportService.save(sr);
        }).flatMap(srSaved -> ServerResponse
                .created(URI.create("/api/v1/service-report"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(srSaved)));

    }

    private Map<String, Object> validateRequest(ServiceReport sr) {
        Map<String, Object> resultValidations = new HashMap<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ServiceReport>> violations = validator.validate(sr);
        if (violations.isEmpty()) {
            int validDates = sr.getStartTime().compareTo(sr.getEndTime());
            long validDatesRangeHours = Duration.between(sr.getStartTime(), sr.getEndTime()).toMinutes();
            if (validDates >= 0) {
                resultValidations.put("error", "startTime could not be greater or equal than endTime");
            } else if (validDatesRangeHours > 720) {
                resultValidations.put("error", "Range in startTime and endTime could not be greater or equal than 12");
            }
        } else {
            resultValidations.put("error", violations);
        }
        return resultValidations;

    }
}

package com.ias.hourscalculatorapi.repository;

import com.ias.hourscalculatorapi.model.ServiceReport;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public interface ServiceReportRepository extends ReactiveMongoRepository<ServiceReport, String> {

    Flux<ServiceReport> findServiceReportsByTechnicianId(String id);
}

package com.ias.hourscalculatorapi.router;

import com.ias.hourscalculatorapi.handler.HoursCalculatorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class HoursCalculatorRouter {

    @Bean
    public RouterFunction<ServerResponse> hoursCalculatorRoutes(HoursCalculatorHandler handler){
        String path = "/api/v1/hours-calculator/{technicianId}/week/{weekNumber}";
        return RouterFunctions.route(RequestPredicates.GET(path), handler::hoursCalculator);
    }
}

package com.ias.hourscalculatorapi.router;

import com.ias.hourscalculatorapi.handler.ServiceReportHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class ServiceReportRouter {

    @Bean
    public RouterFunction<ServerResponse> serviceReportRoutes(ServiceReportHandler handler){
        String path = "api/v1/service-report";
        return RouterFunctions.route(RequestPredicates.POST(path), handler::saveServiceReport);
    }
}

package com.ias.hourscalculatorapi.service;

import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

public interface HoursCalculatorService {
    Mono<List<Map<String, Object>>> calculateHours(String technicianId, Integer weekNumber);
}

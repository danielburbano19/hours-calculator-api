package com.ias.hourscalculatorapi.service;

import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.repository.ServiceReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class HoursCalculatorServiceImpl implements HoursCalculatorService {

    private final LocalTime START_TIME_WORKDAY = LocalTime.of(7, 0);
    private final LocalTime END_TIME_WORKDAY = LocalTime.of(20, 0);
    private final LocalTime START_HOURS_DAY = LocalTime.MIN;
    private final LocalTime END_HOURS_DAY = LocalTime.MAX;

    private final AtomicReference<Float> NORMAL_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> NIGHT_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> SUNDAY_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> EXTRA_NORMAL_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> EXTRA_NIGHT_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> EXTRA_SUNDAY_HOURS = new AtomicReference<>();
    private final AtomicReference<Float> TOTAL_HOURS_WORKED = new AtomicReference<>();
    private final Float INIT_EXTRA_HOURS = 48F;
    private Integer weekNumber;


    private final ServiceReportRepository serviceReportRepository;

    @Override
    public Mono<List<Map<String, Object>>> calculateHours(String technicianId, Integer weekNumber) {
        initHoursCounters();
        this.weekNumber = weekNumber;
        Map<String, Object> result = new HashMap<>();
        result.put("normalHours", NORMAL_HOURS);
        result.put("nightHours", NIGHT_HOURS);
        result.put("sundayHours", SUNDAY_HOURS);
        result.put("extraNormalHours", EXTRA_NORMAL_HOURS);
        result.put("extraNightHours", EXTRA_NIGHT_HOURS);
        result.put("extraSundayHours", EXTRA_SUNDAY_HOURS);

        return serviceReportRepository.findServiceReportsByTechnicianId(technicianId)
                .filter(serviceReport -> isValidWeekInRangeDates(serviceReport.getStartTime(), serviceReport.getEndTime(), weekNumber))
                .map(serviceReport -> {
                    LocalTime startTime = serviceReport.getStartTime().toLocalTime();
                    LocalTime endTime = serviceReport.getEndTime().toLocalTime();
                    if (!validateSundayHours(serviceReport, startTime, endTime)) {
                        validateNormalNightHours(startTime, endTime);
                    }
                    return serviceReport;
                })
                .then(Mono.just(Collections.singletonList(result)));
    }

    private boolean validateSundayHours(ServiceReport serviceReport, LocalTime startTime, LocalTime endTime) {
        Boolean isSundayStartTime = this.isSunday(serviceReport.getStartTime());
        Boolean isSundayEndTime = this.isSunday(serviceReport.getEndTime());
        if (isSundayStartTime || isSundayEndTime) {
            if (!isSundayEndTime) {
                System.out.println("======Sunday to monday=======");
                TemporalField woy = WeekFields.of(Locale.FRANCE).weekOfWeekBasedYear();
                if (this.weekNumber < serviceReport.getEndTime().get(woy)) {
                    addHoursCounters(SUNDAY_HOURS, EXTRA_SUNDAY_HOURS, startTime, END_HOURS_DAY);
                } else {
                    if (endTime.compareTo(START_TIME_WORKDAY) > 0) {
                        addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, START_TIME_WORKDAY, endTime);
                        addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, START_HOURS_DAY, START_TIME_WORKDAY);
                    } else {
                        addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, START_HOURS_DAY, endTime);
                    }
                }
            } else if (!isSundayStartTime) {
                System.out.println("======Saturday to Sunday=======");
                addHoursCounters(SUNDAY_HOURS, EXTRA_SUNDAY_HOURS, START_HOURS_DAY, endTime);
                if (startTime.compareTo(END_TIME_WORKDAY) < 0) {
                    addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, startTime, END_TIME_WORKDAY);
                    addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, END_TIME_WORKDAY, END_HOURS_DAY);
                } else {
                    addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, END_HOURS_DAY);
                }
            } else {
                System.out.println("======Sunday=======");
                addHoursCounters(SUNDAY_HOURS, EXTRA_SUNDAY_HOURS, startTime, endTime);
            }
            return true;
        }
        return false;
    }

    private void validateNormalNightHours(LocalTime startTime, LocalTime endTime) {
        if (startTime.compareTo(START_TIME_WORKDAY) >= 0 && endTime.compareTo(END_TIME_WORKDAY) <= 0 && startTime.compareTo(endTime) < 0) {
            System.out.println("======Normal hours=======");
            addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, startTime, endTime);
        } else if (startTime.compareTo(END_TIME_WORKDAY) >= 0 && endTime.compareTo(END_HOURS_DAY) < 0 && startTime.compareTo(endTime) < 0) {
            System.out.println("======Nocturnal hours - same night=======");
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, endTime);
        } else if (startTime.compareTo(END_TIME_WORKDAY) >= 0 && endTime.compareTo(START_TIME_WORKDAY) < 0 && startTime.compareTo(endTime) > 0) {
            System.out.println("======Nocturnal hours - night early morning=======");
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, END_HOURS_DAY);
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, START_HOURS_DAY, endTime);
        } else if (startTime.compareTo(START_HOURS_DAY) > 0 && endTime.compareTo(START_TIME_WORKDAY) < 0 && startTime.compareTo(endTime) < 0) {
            System.out.println("======Nocturnal hours -  same early morning=======");
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, endTime);
        } else if (startTime.compareTo(END_TIME_WORKDAY) < 0 && endTime.compareTo(END_HOURS_DAY) < 0 && endTime.compareTo(END_TIME_WORKDAY) > 0 && startTime.compareTo(endTime) < 0) {
            System.out.println("======Normal hours - Nocturnal hours - same day=======");
            addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, startTime, END_TIME_WORKDAY);
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, END_TIME_WORKDAY, endTime);
        } else if (startTime.compareTo(END_TIME_WORKDAY) < 0 && endTime.compareTo(START_TIME_WORKDAY) <= 0 && startTime.compareTo(endTime) > 0) {
            System.out.println("======Normal hours - Nocturnal hours - different day=======");
            addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, startTime, END_TIME_WORKDAY);
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, END_TIME_WORKDAY, END_HOURS_DAY);
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, START_HOURS_DAY, endTime);
        } else if (startTime.compareTo(END_TIME_WORKDAY) >= 0 && endTime.compareTo(START_TIME_WORKDAY) >= 0 && startTime.compareTo(endTime) > 0) {
            System.out.println("======Nocturnal hours - Normal hours - night, early morning, normal=======");
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, END_HOURS_DAY);
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, START_HOURS_DAY, START_TIME_WORKDAY);
            addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, START_TIME_WORKDAY, endTime);
        } else if (startTime.compareTo(START_TIME_WORKDAY) < 0 && endTime.compareTo(START_TIME_WORKDAY) > 0 && endTime.compareTo(END_TIME_WORKDAY) < 0 && startTime.compareTo(endTime) < 0) {
            System.out.println("======Nocturnal hours - Normal hours - early morning, normal=======");
            addHoursCounters(NIGHT_HOURS, EXTRA_NIGHT_HOURS, startTime, START_TIME_WORKDAY);
            addHoursCounters(NORMAL_HOURS, EXTRA_NORMAL_HOURS, START_TIME_WORKDAY, endTime);
        }

    }

    private void addHoursCounters(AtomicReference<Float> hoursCounter, AtomicReference<Float> extraHoursCounter, LocalTime startTime, LocalTime endTime) {
        if (isOvertime(TOTAL_HOURS_WORKED)) {
            TOTAL_HOURS_WORKED.updateAndGet(total -> total + calculateTimeBetween(startTime, endTime));
            extraHoursCounter.updateAndGet(h -> h + calculateTimeBetween(startTime, endTime));
            return;
        }
        hoursCounter.updateAndGet(h -> h + calculateTimeBetween(startTime, endTime));
        TOTAL_HOURS_WORKED.updateAndGet(total -> total + calculateTimeBetween(startTime, endTime));
        if (isOvertime(TOTAL_HOURS_WORKED)) {
            Float extraHours = Float.parseFloat(String.valueOf(TOTAL_HOURS_WORKED)) - INIT_EXTRA_HOURS;
            hoursCounter.updateAndGet(h -> h - extraHours);
            extraHoursCounter.updateAndGet(h -> h + extraHours);
        }

    }

    private boolean isOvertime(AtomicReference<Float> hoursCounter) {
        return Float.parseFloat(String.valueOf(hoursCounter)) >= INIT_EXTRA_HOURS;
    }

    private Float calculateTimeBetween(LocalTime startTime, LocalTime endTime) {
        return (float) Math.ceil((Duration.between(startTime, endTime).toMinutes() / 60F));
    }

    private Boolean isValidWeekInRangeDates(LocalDateTime startDateTime, LocalDateTime endDateTime, Integer weekNumber) {
        TemporalField woy = WeekFields.of(Locale.FRANCE).weekOfWeekBasedYear();
        return startDateTime.get(woy) == weekNumber || endDateTime.get(woy) == weekNumber;
    }

    private void initHoursCounters() {
        NORMAL_HOURS.updateAndGet(h -> 0f);
        NIGHT_HOURS.updateAndGet(h -> 0f);
        SUNDAY_HOURS.updateAndGet(h -> 0f);
        EXTRA_NORMAL_HOURS.updateAndGet(h -> 0f);
        EXTRA_NIGHT_HOURS.updateAndGet(h -> 0f);
        EXTRA_SUNDAY_HOURS.updateAndGet(h -> 0f);
        TOTAL_HOURS_WORKED.updateAndGet(h -> 0f);
    }

    private Boolean isSunday(LocalDateTime dateTime) {
        return dateTime.getDayOfWeek().toString().equals("SUNDAY");
    }
}

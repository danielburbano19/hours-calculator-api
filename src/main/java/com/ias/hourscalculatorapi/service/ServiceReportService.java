package com.ias.hourscalculatorapi.service;

import com.ias.hourscalculatorapi.model.ServiceReport;
import reactor.core.publisher.Mono;

public interface ServiceReportService {
    Mono<ServiceReport> save(ServiceReport serviceReport);
}

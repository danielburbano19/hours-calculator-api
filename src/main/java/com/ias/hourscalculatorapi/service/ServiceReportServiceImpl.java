package com.ias.hourscalculatorapi.service;

import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.repository.ServiceReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


@Service
@RequiredArgsConstructor
public class ServiceReportServiceImpl implements ServiceReportService {
    private final ServiceReportRepository serviceReportRepository;
    @Override
    public Mono<ServiceReport> save(ServiceReport serviceReport) {
        return serviceReportRepository.save(serviceReport);
    }
}

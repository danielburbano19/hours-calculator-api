package com.ias.hourscalculatorapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.commons.HourReport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HoursCalculatorApiApplicationTests {

    @Autowired
    private WebTestClient client;

    @Test
    void saveServiceReport() {
        String technicianId = "300";
        String serviceId = "234";
        LocalDateTime startTime = LocalDateTime.parse("2021-04-18T07:00:00");
        LocalDateTime endTime = LocalDateTime.parse("2021-04-18T08:00:00");
        ServiceReport serviceReport = ServiceReport.builder()
                .technicianId(technicianId)
                .serviceId(serviceId)
                .startTime(startTime)
                .endTime(endTime)
                .build();
        client.post().uri("/api/v1/service-report")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(serviceReport), ServiceReport.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(ServiceReport.class)
                .consumeWith(response -> {
                    ServiceReport serviceReportResponse = response.getResponseBody();
                    assert serviceReportResponse != null;
                    Assertions.assertEquals(serviceReportResponse.getTechnicianId(), technicianId);
                    Assertions.assertEquals(serviceReportResponse.getServiceId(), serviceId);
                    Assertions.assertEquals(serviceReportResponse.getStartTime(), startTime);
                    Assertions.assertEquals(serviceReportResponse.getEndTime(), endTime);
                });
    }

    @Test
    void getHoursCalculatorReport() {
        String technicianId = "300";
        String weekNumber = "15";
        client.get().uri("/api/v1/hours-calculator/" + technicianId + "/week/" + weekNumber)
                .exchange()
                .expectStatus().isOk()
                .expectBody(new ParameterizedTypeReference<LinkedHashMap<String, Object>>() {
                })
                .consumeWith(response -> {
                    Object hoursCalculatorReport = response.getResponseBody();
                    HourReport hourReport = new ObjectMapper().convertValue(hoursCalculatorReport, HourReport.class);
                    Assertions.assertTrue(hourReport.getNormalHours() >= 0);
                    Assertions.assertTrue(hourReport.getNightHours() >= 0);
                    Assertions.assertTrue(hourReport.getSundayHours() >= 0);
                    Assertions.assertTrue(hourReport.getExtraNormalHours() >= 0);
                    Assertions.assertTrue(hourReport.getExtraNightHours() >= 0);
                    Assertions.assertTrue(hourReport.getExtraSundayHours() >= 0);
                });
    }

}

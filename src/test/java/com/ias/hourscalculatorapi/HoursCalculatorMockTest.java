package com.ias.hourscalculatorapi;

import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.repository.ServiceReportRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HoursCalculatorMockTest {

    private final String technicianId = "700";
    private final String serviceId = "123";
    private final String weekNumber = "16";

    @Autowired
    private WebTestClient client;

    @MockBean
    private ServiceReportRepository serviceReportRepository;

    @BeforeEach
    public void setup() {

        List<ServiceReport> list = new ArrayList<>();

//      1-  normales 2 - nocturnas 4 - dominicales 5
        LocalDateTime startTime = LocalDateTime.parse("2021-04-24T18:00:00");
        LocalDateTime endTime = LocalDateTime.parse("2021-04-25T05:00:00");
        list = createServiceReport(startTime, endTime, list);

//      2-  normales 0 - nocturnas 3 - dominicales 5
        startTime = LocalDateTime.parse("2021-04-24T21:00:00");
        endTime = LocalDateTime.parse("2021-04-25T05:00:00");
        list = createServiceReport(startTime, endTime, list);

//      3-  normales 0 - nocturnas 0 - dominicales 3
        startTime = LocalDateTime.parse("2021-04-25T06:00:00");
        endTime = LocalDateTime.parse("2021-04-25T09:00:00");
        list = createServiceReport(startTime, endTime, list);

//      4-  normales 0 - nocturnas 5 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-18T21:00:00");
        endTime = LocalDateTime.parse("2021-04-19T05:00:00");
        list = createServiceReport(startTime, endTime, list);

//      5-  normales 0 - nocturnas 0 - dominicales 3
        startTime = LocalDateTime.parse("2021-04-18T21:00:00");
        endTime = LocalDateTime.parse("2021-04-19T09:00:00");
        list = createServiceReport(startTime, endTime, list);

//      6-  normales 0 - nocturnas 5 - dominicales 3
        startTime = LocalDateTime.parse("2021-04-25T21:00:00");
        endTime = LocalDateTime.parse("2021-04-26T05:00:00");
        list = createServiceReport(startTime, endTime, list);

//      7-  normales 2 - nocturnas 7 - dominicales 3
        startTime = LocalDateTime.parse("2021-04-25T21:00:00");
        endTime = LocalDateTime.parse("2021-04-26T09:00:00");
        list = createServiceReport(startTime, endTime, list);

//      8-  normales 11 - nocturnas 2 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T08:00:00");
        endTime = LocalDateTime.parse("2021-04-19T19:00:00");
        list = createServiceReport(startTime, endTime, list);

//      9-  normales 0 - nocturnas 2 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T21:00:00");
        endTime = LocalDateTime.parse("2021-04-19T23:00:00");
        list = createServiceReport(startTime, endTime, list);

//      10-  normales 0 - nocturnas 6 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T21:00:00");
        endTime = LocalDateTime.parse("2021-04-20T03:00:00");
        list = createServiceReport(startTime, endTime, list);

//      11-  normales 0 - nocturnas 2 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T03:00:00");
        endTime = LocalDateTime.parse("2021-04-19T05:00:00");
        list = createServiceReport(startTime, endTime, list);

//      12-  normales 3 - nocturnas 2 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T17:00:00");
        endTime = LocalDateTime.parse("2021-04-19T22:00:00");
        list = createServiceReport(startTime, endTime, list);

//      13-  normales 2 - nocturnas 7 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T18:00:00");
        endTime = LocalDateTime.parse("2021-04-20T03:00:00");
        list = createServiceReport(startTime, endTime, list);

//      14-  normales 1 - nocturnas 10 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T21:00:00");
        endTime = LocalDateTime.parse("2021-04-20T08:00:00");
        list = createServiceReport(startTime, endTime, list);

//      15  normales 1 - nocturnas 4 - dominicales 0
        startTime = LocalDateTime.parse("2021-04-19T03:00:00");
        endTime = LocalDateTime.parse("2021-04-19T08:00:00");
        list = createServiceReport(startTime, endTime, list);

        Flux<ServiceReport> serviceReportFlux = Flux.fromIterable(list);
        Mockito.when(serviceReportRepository.findServiceReportsByTechnicianId(technicianId))
                .thenReturn(serviceReportFlux);
    }

    public List<ServiceReport> createServiceReport(LocalDateTime startTime, LocalDateTime endTime, List<ServiceReport> list) {
        ServiceReport serviceReport = ServiceReport.builder()
                .technicianId(technicianId)
                .serviceId(serviceId)
                .startTime(startTime)
                .endTime(endTime)
                .build();
        list.add(serviceReport);
        return list;
    }

    @Test
    public void getHoursCalculatorReportWhenDataExist() {
        client.get().uri("/api/v1/hours-calculator/" + technicianId + "/week/" + weekNumber)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.normalHours").isEqualTo(10)
                .jsonPath("$.nightHours").isEqualTo(19)
                .jsonPath("$.sundayHours").isEqualTo(19)
                .jsonPath("$.extraNormalHours").isEqualTo(12)
                .jsonPath("$.extraNightHours").isEqualTo(33)
                .jsonPath("$.extraSundayHours").isEqualTo(0);

    }

    @Test
    public void getHoursCalculatorReportWhenDataNotExist() {
        client.get().uri("/api/v1/hours-calculator/700/week/153")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.normalHours").isEqualTo(0)
                .jsonPath("$.nightHours").isEqualTo(0)
                .jsonPath("$.sundayHours").isEqualTo(0)
                .jsonPath("$.extraNormalHours").isEqualTo(0)
                .jsonPath("$.extraNightHours").isEqualTo(0)
                .jsonPath("$.extraSundayHours").isEqualTo(0);
    }

}

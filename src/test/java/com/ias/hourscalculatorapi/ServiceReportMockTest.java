package com.ias.hourscalculatorapi;

import com.ias.hourscalculatorapi.model.ServiceReport;
import com.ias.hourscalculatorapi.repository.ServiceReportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ServiceReportMockTest {

    @Autowired
    private WebTestClient client;

    @MockBean
    private ServiceReportRepository serviceReportRepository;

    @Test
    public void saveServiceReportWhenDataIsCorrect(){
        String technicianId = "700";
        String serviceId = "234";
        LocalDateTime startTime = LocalDateTime.parse("2021-04-18T07:00:00");
        LocalDateTime endTime = LocalDateTime.parse("2021-04-18T08:00:00");

        ServiceReport serviceReport = ServiceReport.builder()
                .technicianId(technicianId)
                .serviceId(serviceId)
                .startTime(startTime)
                .endTime(endTime)
                .build();

        Mockito.when(serviceReportRepository.save(serviceReport))
                .thenReturn(Mono.just(serviceReport));

        client.post().uri("/api/v1/service-report")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(serviceReport), ServiceReport.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(ServiceReport.class)
                .consumeWith(response -> {
                    ServiceReport serviceReportResponse = response.getResponseBody();
                    assert serviceReportResponse != null;
                    Assertions.assertEquals(serviceReportResponse.getTechnicianId(), technicianId);
                    Assertions.assertEquals(serviceReportResponse.getServiceId(), serviceId);
                    Assertions.assertEquals(serviceReportResponse.getStartTime(), startTime);
                    Assertions.assertEquals(serviceReportResponse.getEndTime(), endTime);
                });
    }

    @Test
    public void saveServiceReportWhenDataIsEmpty(){
        ServiceReport serviceReport = ServiceReport.builder()
                .build();

        Mockito.when(serviceReportRepository.save(serviceReport))
                .thenReturn(Mono.just(serviceReport));

        client.post().uri("/api/v1/service-report")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(serviceReport), ServiceReport.class)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    public void saveServiceReportWhenStartDateIsGreaterThanEndDate(){
        String technicianId = "700";
        String serviceId = "234";
        LocalDateTime startTime = LocalDateTime.parse("2021-04-18T10:00:00");
        LocalDateTime endTime = LocalDateTime.parse("2021-04-18T08:00:00");

        ServiceReport serviceReport = ServiceReport.builder()
                .technicianId(technicianId)
                .serviceId(serviceId)
                .startTime(startTime)
                .endTime(endTime)
                .build();

        Mockito.when(serviceReportRepository.save(serviceReport))
                .thenReturn(Mono.just(serviceReport));

        client.post().uri("/api/v1/service-report")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(serviceReport), ServiceReport.class)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    public void saveServiceReportWhenDatesDifferenceIsGreaterThan12Hours(){
        String technicianId = "700";
        String serviceId = "234";
        LocalDateTime startTime = LocalDateTime.parse("2021-04-18T08:00");
        LocalDateTime endTime = LocalDateTime.parse("2021-04-18T20:01");

        ServiceReport serviceReport = ServiceReport.builder()
                .technicianId(technicianId)
                .serviceId(serviceId)
                .startTime(startTime)
                .endTime(endTime)
                .build();

        Mockito.when(serviceReportRepository.save(serviceReport))
                .thenReturn(Mono.just(serviceReport));

        client.post().uri("/api/v1/service-report")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(serviceReport), ServiceReport.class)
                .exchange()
                .expectStatus().isBadRequest();
    }

}
